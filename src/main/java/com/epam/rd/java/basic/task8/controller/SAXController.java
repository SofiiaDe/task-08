package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    private static final List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

        SAXParser saxParser = saxParserFactory.newSAXParser();
        saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        saxParser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

        FlowerHandler handler = new FlowerHandler();

        saxParser.parse(new File(xmlFileName), handler);

        return flowers;
    }

    private static class FlowerHandler extends DefaultHandler {

        private String name;
        private String soil;
        private String origin;
        private String stemColour;
        private String leafColour;
        private String aveLenFlower;
        private String tempreture;
        private String lightRequiring;
        private String mlPerWeek;
        private String multiplying;

        private String element;

        @Override
        public void characters(char[] ch, int start, int length) {
            String content = new String(ch, start, length); // get current text content from entire XML file
            content = content.replace("\n", "").strip();

            if (!content.isBlank()) {

                switch (element) {
                    case "name":
                        name = content;
                        break;
                    case "soil":
                        soil = content;
                        break;
                    case "origin":
                        origin = content;
                        break;
                    case "stemColour":
                        stemColour = content;
                        break;
                    case "leafColour":
                        leafColour = content;
                        break;
                    case "aveLenFlower":
                        aveLenFlower = content;
                        break;
                    case "tempreture":
                        tempreture = content;
                        break;
                    case "watering":
                        mlPerWeek = content;
                        break;
                    case "multiplying":
                        multiplying = content;
                        break;
                }
            }
        }

        /**
         * iterate over all the elements and convert its text content to characters
         */
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attr) {
            element = qName.intern(); // add string to pool (for fast comparison)
            if ("lighting".equals(element)) { // if element "lighting" need to read only it's first attribute
                lightRequiring = attr.getValue(0);
            }
        }

        /**
         * is invoked when the parsing ends for an element –
         * this is when we'll assign the content of the tags to their respective variables.
         * We can create a flower and add it to the flowers list
         */
        @Override
        public void endElement(String uri, String localName, String qName) {
            if ((nonNull(name) && !name.isBlank()) &&
                    (nonNull(soil) && !soil.isBlank()) &&
                    (nonNull(origin) && !origin.isBlank()) &&
                    (nonNull(stemColour) && !stemColour.isBlank()) &&
                    (nonNull(leafColour) && !leafColour.isBlank()) &&
                    (nonNull(aveLenFlower) && !aveLenFlower.isBlank()) &&
                    (nonNull(tempreture) && !tempreture.isBlank()) &&
                    (nonNull(mlPerWeek) && !mlPerWeek.isBlank()) &&
                    (nonNull(multiplying) && !multiplying.isBlank())) {

                Flower flower = new Flower();
                flower.setName(name);
                flower.setSoil(soil);
                flower.setOrigin(origin);
                flower.setStemColour(stemColour);
                flower.setLeafColour(leafColour);
                flower.setAveLenFlower(Integer.parseInt(aveLenFlower));
                flower.setTempreture(Integer.parseInt(tempreture));
                flower.setLightRequiring(lightRequiring);
                flower.setMlPerWeek(Integer.parseInt(mlPerWeek));
                flower.setMultiplying(multiplying);

                flowers.add(flower);

                name = null;
                soil = null;
                origin = null;
                stemColour = null;
                leafColour = null;
                aveLenFlower = null;
                tempreture = null;
                lightRequiring = null;
                mlPerWeek = null;
                multiplying = null;
            }
        }
    }
}