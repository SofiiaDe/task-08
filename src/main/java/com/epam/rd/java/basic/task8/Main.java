package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.controller.Flower;

import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> domFlowers = domController.getFlowers();


		// sort (case 1)
		// PLACE YOUR CODE HERE
		domFlowers.sort(Comparator.comparing(Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		ObjectsToXMLSaver.saveObjectsToXML(domFlowers, outputXmlFile);



		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> saxFlowers = saxController.getFlowers();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		saxFlowers.sort(Comparator.comparing(Flower::getName).reversed());

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		ObjectsToXMLSaver.saveObjectsToXML(saxFlowers, outputXmlFile);


		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> staxFlowers = staxController.getFlowers();

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		staxFlowers.sort(Comparator.comparing(Flower::getMlPerWeek));


		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		ObjectsToXMLSaver.saveObjectsToXML(staxFlowers, outputXmlFile);
	}

}
