package com.epam.rd.java.basic.task8.controller;


import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public List<Flower> getFlowers() throws ParserConfigurationException, IOException, SAXException {

        List<Flower> flowers = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document document = docBuilder.parse(new File(xmlFileName));
        document.getDocumentElement().normalize();

        // getting a list of <flower> child elements
        NodeList flowerElements = document.getElementsByTagName("flower");

        for (int i = 0; i < flowerElements.getLength(); i++) {
            Element flowerElement = (Element) flowerElements.item(i);

            // get all values of flower element
            String[] flowerValues = flowerElement.getTextContent().lines()
                    .map(String::strip)
                    .filter(e -> !e.isBlank())
                    .toArray(String[]::new);

            String name = flowerValues[0];
            String soil = flowerValues[1];
            String origin = flowerValues[2];
            String stemColour = flowerValues[3];
            String leafColour = flowerValues[4];
            String aveLenFlower = flowerValues[5];
            String tempreture = flowerValues[6];
            String lighting = flowerElement.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent();
            String watering = flowerValues[7];
            String multiplying = flowerValues[8];

            Flower flower = new Flower();
            flower.setName(name);
            flower.setSoil(soil);
            flower.setOrigin(origin);
            flower.setStemColour(stemColour);
            flower.setLeafColour(leafColour);
            flower.setAveLenFlower(Integer.parseInt(aveLenFlower));
            flower.setTempreture(Integer.parseInt(tempreture));
            flower.setLightRequiring(lighting);
            flower.setMlPerWeek(Integer.parseInt(watering));
            flower.setMultiplying(multiplying);

            flowers.add(flower);
        }

        return flowers;
    }
}