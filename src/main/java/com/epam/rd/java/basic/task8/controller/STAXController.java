package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public List<Flower> getFlowers() throws FileNotFoundException, XMLStreamException {

        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
        XMLEventReader eventReader = inputFactory.createXMLEventReader(new FileInputStream(xmlFileName));

        List<Flower> flowers = new ArrayList<>();
        Flower flower = new Flower();

        while (eventReader.hasNext()) {
            XMLEvent nextEvent = eventReader.nextEvent();
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "flower":
                        flower = new Flower();
                        break;
                    case "name":
                        nextEvent = eventReader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case "soil":
                        nextEvent = eventReader.nextEvent();
                        flower.setSoil(nextEvent.asCharacters().getData());
                        break;
                    case "origin":
                        nextEvent = eventReader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case "stemColour":
                        nextEvent = eventReader.nextEvent();
                        flower.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case "leafColour":
                        nextEvent = eventReader.nextEvent();
                        flower.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case "aveLenFlower":
                        nextEvent = eventReader.nextEvent();
                        flower.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "tempreture":
                        nextEvent = eventReader.nextEvent();
                        flower.setTempreture(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "lighting":
                        nextEvent = eventReader.nextEvent();
                        flower.setLightRequiring(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
                        break;
                    case "watering":
                        nextEvent = eventReader.nextEvent();
                        flower.setMlPerWeek(Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "multiplying":
                        nextEvent = eventReader.nextEvent();
                        flower.setMultiplying(nextEvent.asCharacters().getData());
                        break;
                }
            }
            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("flower")) {
                    flowers.add(flower);
                }
            }


        }
        return flowers;

    }
}