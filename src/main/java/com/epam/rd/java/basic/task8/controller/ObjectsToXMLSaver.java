package com.epam.rd.java.basic.task8.controller;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ObjectsToXMLSaver {

    private static final String MEASURE = "measure";

    public static void saveObjectsToXML(List<Flower> flowers, String outputXMLFile) throws
            ParserConfigurationException, TransformerException, IOException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document document = docBuilder.newDocument();

        // create and add element to XML file
        Element rootElement = document.createElement("flowers");

        //set necessary attributes
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        document.appendChild(rootElement);

        for (Flower flower : flowers) {

            Element elementFlower = document.createElement("flower");
            rootElement.appendChild(elementFlower);

            Element name = document.createElement("name");
            name.setTextContent(flower.getName());
            elementFlower.appendChild(name);

            Element soil = document.createElement("soil");
            soil.setTextContent(flower.getSoil());
            elementFlower.appendChild(soil);

            Element origin = document.createElement("origin");
            origin.setTextContent(flower.getOrigin());
            elementFlower.appendChild(origin);

            Element visualParameters = document.createElement("visualParameters");

            Element stemColour = document.createElement("stemColour");
            stemColour.setTextContent(flower.getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = document.createElement("leafColour");
            leafColour.setTextContent(flower.getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = document.createElement("aveLenFlower");
            aveLenFlower.setAttribute(MEASURE, "cm");
            aveLenFlower.setTextContent(String.valueOf(flower.getAveLenFlower()));
            visualParameters.appendChild(aveLenFlower);

            elementFlower.appendChild(visualParameters);

            Element growingTips = document.createElement("growingTips");

            Element tempreture = document.createElement("tempreture");
            tempreture.setAttribute(MEASURE, "celcius");
            tempreture.setTextContent(String.valueOf(flower.getTempreture()));
            growingTips.appendChild(tempreture);

            Element lighting = document.createElement("lighting");
            lighting.setAttribute("lightRequiring", flower.getLightRequiring());
            growingTips.appendChild(lighting);

            Element watering = document.createElement("watering");
            watering.setAttribute(MEASURE, "mlPerWeek");
            watering.setTextContent(String.valueOf(flower.getMlPerWeek()));
            growingTips.appendChild(watering);

            elementFlower.appendChild(growingTips);

            Element multiplying = document.createElement("multiplying");
            multiplying.setTextContent(flower.getMultiplying());
            elementFlower.appendChild(multiplying);
        }

        // write document to a file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new FileWriter(outputXMLFile));

        transformer.transform(source, result);

    }
}